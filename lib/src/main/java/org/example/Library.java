package org.example;

import com.google.common.base.Strings;

public class Library {

    public boolean someLibraryMethod() {
        return true;
    }

    public String greet(String name) {
        return "Hello, " + Strings.nullToEmpty(name) + "!";
    }
}
